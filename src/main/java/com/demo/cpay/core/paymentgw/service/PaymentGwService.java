package com.demo.cpay.core.paymentgw.service;

import com.demo.cpay.core.paymentgw.vo.req.OrderReq;
import com.demo.cpay.core.paymentgw.vo.req.QueryReq;
import com.demo.cpay.core.paymentgw.vo.res.OrderRes;
import com.demo.cpay.core.paymentgw.vo.res.QueryRes;

public interface PaymentGwService {

    OrderRes order(OrderReq orderReq);

    QueryRes orderquery(QueryReq queryReq);
}
