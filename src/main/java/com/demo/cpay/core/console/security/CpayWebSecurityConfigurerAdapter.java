package com.demo.cpay.core.console.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class CpayWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

	@Autowired
    private DataSource dataSource;
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		
		http.csrf().disable()
        		.authorizeRequests().antMatchers("/console/**").access("hasRole('ROLE_ADMIN')").anyRequest().authenticated()
        		.and()
        			.formLogin()
        			.loginPage("/")
        			.loginProcessingUrl("/login").permitAll()
        			.successHandler(vpayAuthenticationSuccessHandler())
        			.failureHandler(vpayAuthenticationFailHandler())
        		.and()
        			.logout()
        			.logoutUrl("/logout")
        			.logoutSuccessUrl("/")
            .and()
            .httpBasic();
		
    }

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery("select username, password, active from ADM_USER where username=?")
                .authoritiesByUsernameQuery("select au.username, ar.role from ADM_USER au join ADM_ROLE ar on au.role_code=ar.role_code where au.username=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder());
    }
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.debug(true);
	    web
	       .ignoring()
	       .antMatchers("/openapi/**","/payment/**","/loginErr/**", "/forgotpwd/**", "/bootstrap/**", "/dist/**", "/fastclick/**", "/font-awesome/**", "/Ionicons/**", "/jquery/**", "/plugins/**", "/datatables/**");
	}

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    public CpayAuthenticationSuccessHandler vpayAuthenticationSuccessHandler() {
        return new CpayAuthenticationSuccessHandler();
    }
    
    @Bean
    public CpayAuthenticationFailHandler vpayAuthenticationFailHandler() {
        return new CpayAuthenticationFailHandler();
    }
    
}
