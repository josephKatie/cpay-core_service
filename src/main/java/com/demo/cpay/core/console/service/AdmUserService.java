package com.demo.cpay.core.console.service;

import com.demo.cpay.core.console.entity.AdmUser;

public interface AdmUserService {

    AdmUser getAdmUserByUsername(String username);

    int updateErrLoginCnt(String username, boolean loginSuccess);
}
