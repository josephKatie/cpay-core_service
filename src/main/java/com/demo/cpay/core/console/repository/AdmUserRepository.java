package com.demo.cpay.core.console.repository;

import com.demo.cpay.core.console.entity.AdmUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdmUserRepository extends JpaRepository<AdmUser, String> {

    AdmUser getAdmUserByUsername(String username);
}
