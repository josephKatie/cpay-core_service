package com.demo.cpay.core.console.service;

import com.demo.cpay.core.console.entity.TrxRecord;
import org.springframework.data.domain.Page;

public interface TrxRecordService {

    Page<TrxRecord> findByPage(int page, int pagesize);

    void save(TrxRecord trxRecord);

    TrxRecord findById(String trxCode);
}
