package com.demo.cpay.core.console.repository;

import com.demo.cpay.core.console.entity.MerTppSetting;
import com.demo.cpay.base.enums.ChannelType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MerTppSettingRepository extends JpaRepository<MerTppSetting, String> {

    List<MerTppSetting> findByChannelType(ChannelType channelType);
}
